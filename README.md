# Advent of Code 2021
Here are my code solutions for the AOC 2021 (https://adventofcode.com/2021)

This year is my first attempt at participating in the live event.

I try to solve each days' puzzle as fast as possible, typically getting the solution first in a clumsy way. Then
some refactoring to clean up the code and make it more expressive and/or show the intent in an easier way.

I have published some pages on bitbucket where I describe my solutions. You can find the pages on https://jeanmarc-nl.bitbucket.io/aoc2021/index.html
