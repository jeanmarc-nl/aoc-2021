package nl.about42.aoc2021

class day18Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("18", true)
    lines should not be empty
  }
}
