package nl.about42.aoc2021

class day07Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("07", true)
    lines should not be empty
  }
}
