package nl.about42.aoc2021

class day17Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("17", true)
    lines should not be empty
  }
}
