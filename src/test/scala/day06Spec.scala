package nl.about42.aoc2021

class day06Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("06", true)
    lines should not be empty
  }
}
