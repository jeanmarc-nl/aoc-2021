package nl.about42.aoc2021

class day24Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("24", true)
    lines should not be empty
  }
}
