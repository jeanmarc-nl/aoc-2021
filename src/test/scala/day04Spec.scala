package nl.about42.aoc2021

class day04Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("04", true)
    lines should not be empty
  }
}
