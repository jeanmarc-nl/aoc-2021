package nl.about42.aoc2021

class day20Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("20", true)
    lines should not be empty
  }
}
