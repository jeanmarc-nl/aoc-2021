package nl.about42.aoc2021

class day10Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("10", true)
    lines should not be empty
  }
}
