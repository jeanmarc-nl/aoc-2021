package nl.about42.aoc2021

class day21Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("21", true)
    lines should not be empty
  }
}
