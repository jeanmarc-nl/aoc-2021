package nl.about42.aoc2021

class day16Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("16", true)
    lines should not be empty
  }
}
