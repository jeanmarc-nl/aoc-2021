package nl.about42.aoc2021

class day13Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("13", true)
    lines should not be empty
  }
}
