package nl.about42.aoc2021

class day05Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("05", true)
    lines should not be empty
  }
}
