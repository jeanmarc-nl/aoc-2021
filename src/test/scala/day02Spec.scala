package nl.about42.aoc2021

class day02Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("02", true)
    lines should not be empty
  }
}
