package nl.about42.aoc2021

class day01Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("01", true)
    lines should not be empty
  }
}
