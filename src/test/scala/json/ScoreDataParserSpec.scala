package nl.about42.aoc2021
package json

import scoredata.{Board, Member, Star}

import io.circe.parser._
import ScoreDataParser._

class ScoreDataParserSpec extends BaseSpec {
  "A Board JSON" should "be parsable" in {

    val in = """|{
        |   "event": "2021",
        |   "owner_id": "1502979",
        |   "members": {
        |       "999999": {
        |           "id": "999999",
        |           "last_star_ts": 1638783592,
        |           "local_score": 211,
        |           "completion_day_level": {
        |             "2": {
        |               "1": {
        |                 "get_star_ts": 1638451665
        |               },
        |               "2": {
        |                 "get_star_ts": 1638452052
        |               }
        |             },
        |             "5": {
        |               "1": {
        |                 "get_star_ts": 1638703508
        |               },
        |               "2": {
        |                 "get_star_ts": 1638703911
        |               }
        |             }
        |           },
        |           "stars": 12,
        |           "global_score": 0,
        |           "name": "Somebody"
        |       },
        |       "888888": {
        |           "last_star_ts": 1638370036,
        |           "name": null,
        |           "global_score": 0,
        |           "stars": 2,
        |           "completion_day_level": {
        |             "1": {
        |               "1": {
        |                 "get_star_ts": 1638369574
        |               },
        |               "2": {
        |                 "get_star_ts": 1638370036
        |               }
        |             }
        |           },
        |           "local_score": 24,
        |           "id": "888888"
        |       }
        |   }
        | }
        |   """.stripMargin

    val s = decode[Board](in)

    s match {
      case Left(e) =>
        println(s"$e")
        fail()
      case Right(r) =>
        val exp = Board(
          "2021",
          "1502979",
          Map(
            (
              "999999",
              Member(
                "999999",
                1638783592,
                211,
                Map(
                  "2" -> Map(
                    "1" -> Star(1638451665),
                    "2" -> Star(1638452052)
                  ),
                  "5" -> Map(
                    "1" -> Star(1638703508),
                    "2" -> Star(1638703911)
                  )
                ),
                12,
                0,
                Some("Somebody")
              )
            ),
            (
              "888888",
              Member(
                "888888",
                1638370036,
                24,
                Map(
                  "1" -> Map(
                    "1" -> Star(1638369574),
                    "2" -> Star(1638370036)
                  )
                ),
                2,
                0,
                None
              )
            )
          )
        )
        r should be(exp)
    }
  }
}
