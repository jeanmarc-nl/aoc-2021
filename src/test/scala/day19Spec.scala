package nl.about42.aoc2021

class day19Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("19", true)
    lines should not be empty
  }
}
