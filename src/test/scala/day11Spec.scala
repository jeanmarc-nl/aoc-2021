package nl.about42.aoc2021

class day11Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("11", true)
    lines should not be empty
  }
}
