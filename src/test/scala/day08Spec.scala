package nl.about42.aoc2021

class day08Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("08", true)
    lines should not be empty
  }
}
