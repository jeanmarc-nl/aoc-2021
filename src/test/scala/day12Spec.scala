package nl.about42.aoc2021

class day12Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("12", true)
    lines should not be empty
  }
}
