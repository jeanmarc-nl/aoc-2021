package nl.about42.aoc2021

import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

abstract trait BaseSpec extends AnyFlatSpec with Matchers with GivenWhenThen with InputHandler {}
