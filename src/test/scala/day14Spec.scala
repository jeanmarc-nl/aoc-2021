package nl.about42.aoc2021

class day14Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("14", true)
    lines should not be empty
  }
}
