package nl.about42.aoc2021

class day23Spec extends BaseSpec {
  "An input file" should "be read" in {
    val lines = getInputForDay("23", true)
    lines should not be empty
  }
}
