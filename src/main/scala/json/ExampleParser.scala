package nl.about42.aoc2021
package json

import io.circe.generic.semiauto.deriveDecoder
import io.circe.{ACursor, Decoder, Json}
import io.circe.jawn.decode

object ExampleParser {
  val jsonMember = """{"id":"A", "name":"Aname"}"""
  val jsonMemberNoID = """{"name":"Aname"}"""
  val json =
    """{ "members": { "id1": { "name": "Foo" }, "id2": {"name": "bar" }, "id3": {"id":"ID3", "name":"baz"} } }"""
  case class Member(id: String, name: String)
  case class Members(members: Map[String, Member])

  implicit val MemberDecoder: Decoder[Member] = deriveDecoder[Member].prepare {
    (aCursor: ACursor) =>
      {
        aCursor.withFocus(json => {
          json.mapObject(jsonObject => {
            if (!jsonObject.contains("id")) {
              jsonObject.add("id", Json.fromString(aCursor.key.getOrElse("?")))
            } else {
              jsonObject
            }
          })
        })
      }
  }

  implicit val MembersDecoder: Decoder[Members] = deriveDecoder[Members]

  val memberResult = decode[Member](jsonMember)
  val result = decode[Members](json)
  val m2 = decode[Member](jsonMemberNoID)
}
