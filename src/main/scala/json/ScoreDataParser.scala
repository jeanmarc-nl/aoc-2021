package nl.about42.aoc2021
package json

import scoredata.{Board, Member, Star}

import io.circe.generic.semiauto.deriveDecoder
import io.circe.{ACursor, Decoder, Json}

object ScoreDataParser {

  implicit val StarDecoder: Decoder[Star] = deriveDecoder[Star]

  implicit val MemberDecoder: Decoder[Member] =
    deriveDecoder[Member].prepare((aC: ACursor) => {
      aC.withFocus(json => {
        json.mapObject(obj => {
          if (!obj.contains("id")) {
            obj.add("id", Json.fromString(aC.key.getOrElse("?")))
          } else {
            obj
          }
        })
      })
    })

  implicit val BoardDecoder: Decoder[Board] = deriveDecoder[Board]
}
