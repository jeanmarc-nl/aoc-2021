package nl.about42.aoc2021

import Amphipod.Amphipod

import scala.collection.mutable

object Amphipod extends Enumeration {
  type Amphipod = Value
  val Amber = Value(1)
  val Bronze = Value(10)
  val Copper = Value(100)
  val Desert = Value(1000)

  def fromChar(c: Char): Amphipod = c match {
    case 'A' => Amber
    case 'B' => Bronze
    case 'C' => Copper
    case 'D' => Desert
  }

  def color(amphipod: Amphipod): Char = amphipod match {
    case Amber  => 'A'
    case Bronze => 'B'
    case Copper => 'C'
    case Desert => 'D'
  }
}

case class Move(from: Int, to: Int, creature: Amphipod) {
  def cost = {
    val steps = (Math.min(from, to), Math.max(from, to)) match {
      case (0, 16) => 6
      case (0, 17) => 5
      case (0, 18) => 5
      case (0, 19) => 7
      case (0, 20) => 9
      case (0, 21) => 11
      case (0, 22) => 12
      case (1, 16) => 5
      case (1, 17) => 4
      case (1, 18) => 4
      case (1, 19) => 6
      case (1, 20) => 8
      case (1, 21) => 10
      case (1, 22) => 11
      case (2, 16) => 4
      case (2, 17) => 3
      case (2, 18) => 3
      case (2, 19) => 5
      case (2, 20) => 7
      case (2, 21) => 9
      case (2, 22) => 10
      case (3, 16) => 3
      case (3, 17) => 2
      case (3, 18) => 2
      case (3, 19) => 4
      case (3, 20) => 6
      case (3, 21) => 8
      case (3, 22) => 9

      case (4, 16) => 8
      case (4, 17) => 7
      case (4, 18) => 5
      case (4, 19) => 5
      case (4, 20) => 7
      case (4, 21) => 9
      case (4, 22) => 10
      case (5, 16) => 7
      case (5, 17) => 6
      case (5, 18) => 4
      case (5, 19) => 4
      case (5, 20) => 6
      case (5, 21) => 8
      case (5, 22) => 9
      case (6, 16) => 6
      case (6, 17) => 5
      case (6, 18) => 3
      case (6, 19) => 3
      case (6, 20) => 5
      case (6, 21) => 7
      case (6, 22) => 8
      case (7, 16) => 5
      case (7, 17) => 4
      case (7, 18) => 2
      case (7, 19) => 2
      case (7, 20) => 4
      case (7, 21) => 6
      case (7, 22) => 7

      case (8, 16)  => 10
      case (8, 17)  => 9
      case (8, 18)  => 7
      case (8, 19)  => 5
      case (8, 20)  => 5
      case (8, 21)  => 7
      case (8, 22)  => 8
      case (9, 16)  => 9
      case (9, 17)  => 8
      case (9, 18)  => 6
      case (9, 19)  => 4
      case (9, 20)  => 4
      case (9, 21)  => 6
      case (9, 22)  => 7
      case (10, 16) => 8
      case (10, 17) => 7
      case (10, 18) => 5
      case (10, 19) => 3
      case (10, 20) => 3
      case (10, 21) => 5
      case (10, 22) => 6
      case (11, 16) => 7
      case (11, 17) => 6
      case (11, 18) => 4
      case (11, 19) => 2
      case (11, 20) => 2
      case (11, 21) => 4
      case (11, 22) => 5

      case (12, 16) => 12
      case (12, 17) => 11
      case (12, 18) => 9
      case (12, 19) => 7
      case (12, 20) => 5
      case (12, 21) => 5
      case (12, 22) => 6
      case (13, 16) => 11
      case (13, 17) => 10
      case (13, 18) => 8
      case (13, 19) => 6
      case (13, 20) => 4
      case (13, 21) => 4
      case (13, 22) => 5
      case (14, 16) => 10
      case (14, 17) => 9
      case (14, 18) => 7
      case (14, 19) => 5
      case (14, 20) => 3
      case (14, 21) => 3
      case (14, 22) => 4
      case (15, 16) => 9
      case (15, 17) => 8
      case (15, 18) => 6
      case (15, 19) => 4
      case (15, 20) => 2
      case (15, 21) => 2
      case (15, 22) => 3
      case (_, _)   => 100000
    }
    steps * creature.id
  }
}

object BurrowBoard {
  val burrowMap = Map(
    0 -> (5, 3),
    1 -> (4, 3),
    2 -> (3, 3),
    3 -> (2, 3),
    4 -> (5, 5),
    5 -> (4, 5),
    6 -> (3, 5),
    7 -> (2, 5),
    8 -> (5, 7),
    9 -> (4, 7),
    10 -> (3, 7),
    11 -> (2, 7),
    12 -> (5, 9),
    13 -> (4, 9),
    14 -> (3, 9),
    15 -> (2, 9),
    16 -> (1, 1),
    17 -> (1, 2),
    18 -> (1, 4),
    19 -> (1, 6),
    20 -> (1, 8),
    21 -> (1, 10),
    22 -> (1, 11)
  )
  def showBoard(data: String) = {
    val burrow = Array(
      "#############".toArray,
      "#           #".toArray,
      "### # # # ###".toArray,
      "  # # # # #".toArray,
      "  # # # # #".toArray,
      "  # # # # #".toArray,
      "  #########".toArray
    )
    data.zipWithIndex.foreach { case (c, i) =>
      burrow(burrowMap(i)._1)(burrowMap(i)._2) = c
    }
    burrow.foreach(row => println(row.mkString))
  }

  def fromInput(lines: List[String]): String = {
    // extract board position
    val places = List(
      lines(5)(3),
      lines(4)(3),
      lines(3)(3),
      lines(2)(3),
      lines(5)(5),
      lines(4)(5),
      lines(3)(5),
      lines(2)(5),
      lines(5)(7),
      lines(4)(7),
      lines(3)(7),
      lines(2)(7),
      lines(5)(9),
      lines(4)(9),
      lines(3)(9),
      lines(2)(9)
    )
    places.mkString + "       "
  }

  def isFinish(board: String) = board.startsWith("AAAABBBBCCCCDDDD")

  def findPossibleMoves(board: String): List[Move] = {
    val (inFree, outFree) = freeSpots(board).partition(_ < 16)
    val (inCandidates, outCandidates) = movableAmphipods(board).partition(_ < 16)

    //    BurrowBoard.showBoard(board)
    //    println(s"inFree: $inFree")
    //    println(s"outFree: $outFree")
    //    println(s"inCandidates: $inCandidates")
    //    println(s"outCandidates: $outCandidates")

    // legal moves are from in to out, or from out to in (but only to the correct color)

    // in to out: only if not yet at the correct position with spots below it filled correctly too
    val movableInCandidates = inCandidates
      .map {
        case 0  => (0, board(0) != 'A')
        case 1  => (1, board(1) != 'A' || board(0) != 'A')
        case 2  => (2, board(2) != 'A' || board(1) != 'A' || board(0) != 'A')
        case 3  => (3, board(3) != 'A' || board(2) != 'A' || board(1) != 'A' || board(0) != 'A')
        case 4  => (4, board(4) != 'B')
        case 5  => (5, board(5) != 'B' || board(4) != 'B')
        case 6  => (6, board(6) != 'B' || board(5) != 'B' || board(4) != 'B')
        case 7  => (7, board(7) != 'B' || board(6) != 'B' || board(5) != 'B' || board(4) != 'B')
        case 8  => (8, board(8) != 'C')
        case 9  => (9, board(9) != 'C' || board(8) != 'C')
        case 10 => (10, board(10) != 'C' || board(9) != 'C' || board(8) != 'C')
        case 11 => (11, board(11) != 'C' || board(10) != 'C' || board(9) != 'C' || board(8) != 'C')
        case 12 => (12, board(12) != 'D')
        case 13 => (13, board(13) != 'D' || board(12) != 'D')
        case 14 => (14, board(14) != 'D' || board(13) != 'D' || board(12) != 'D')
        case 15 => (15, board(15) != 'D' || board(14) != 'D' || board(13) != 'D' || board(12) != 'D')
        case p  => (p, false)
      }
      .filter(_._2)
      .map(_._1)

    //println(s"movableIn: $movableInCandidates")

    // out to in: only to the lowest free spot, and only if the lower spots are correctly filled
    val availableInSpots = inFree
      .map {
        case 0  => (0, true)
        case 1  => (1, board(0) == 'A')
        case 2  => (2, board(0) == 'A' && board(1) == 'A')
        case 3  => (3, board(0) == 'A' && board(1) == 'A' && board(2) == 'A')
        case 4  => (4, true)
        case 5  => (5, board(4) == 'B')
        case 6  => (6, board(4) == 'B' && board(5) == 'B')
        case 7  => (7, board(4) == 'B' && board(5) == 'B' && board(6) == 'B')
        case 8  => (8, true)
        case 9  => (9, board(8) == 'C')
        case 10 => (10, board(8) == 'C' && board(9) == 'C')
        case 11 => (11, board(8) == 'C' && board(9) == 'C' && board(10) == 'C')
        case 12 => (12, true)
        case 13 => (13, board(12) == 'D')
        case 14 => (14, board(12) == 'D' && board(13) == 'D')
        case 15 => (15, board(12) == 'D' && board(13) == 'D' && board(14) == 'D')
        case p  => (p, false)
      }
      .filter(_._2)
      .map(_._1)

    //println(s"availableIn: $availableInSpots")

    val inMoves = (for {
      from <- outCandidates
      to <- availableInSpots
    } yield (from, to)).filter(p => canReach(p._1, p._2, board))

    val outMoves = (for {
      from <- movableInCandidates
      to <- outFree
    } yield (from, to)).filter(p => canReach(p._1, p._2, board))

    //    println(s"inMoves: $inMoves")
    //    println(s"outMoves: $outMoves")
    (inMoves ++ outMoves).map(m => Move(m._1, m._2, Amphipod.fromChar(board(m._1))))
  }

  def canReach(from: Int, to: Int, board: String): Boolean = {
    (from, to) match {
      case (0, 16) => board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(17) == ' '
      case (0, 17) => board(1) == ' ' && board(2) == ' ' && board(3) == ' '
      case (0, 18) => board(1) == ' ' && board(2) == ' ' && board(3) == ' '
      case (0, 19) => board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' '
      case (0, 20) => board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' '
      case (0, 21) => board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (0, 22) => board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (1, 16) => board(2) == ' ' && board(3) == ' ' && board(17) == ' '
      case (1, 17) => board(2) == ' ' && board(3) == ' '
      case (1, 18) => board(2) == ' ' && board(3) == ' '
      case (1, 19) => board(2) == ' ' && board(3) == ' ' && board(18) == ' '
      case (1, 20) => board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' '
      case (1, 21) => board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (1, 22) => board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (2, 16) => board(3) == ' ' && board(17) == ' '
      case (2, 17) => board(3) == ' '
      case (2, 18) => board(3) == ' '
      case (2, 19) => board(3) == ' ' && board(18) == ' '
      case (2, 20) => board(3) == ' ' && board(18) == ' ' && board(19) == ' '
      case (2, 21) => board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (2, 22) => board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (3, 16) => board(17) == ' '
      case (3, 17) => true
      case (3, 18) => true
      case (3, 19) => board(18) == ' '
      case (3, 20) => board(18) == ' ' && board(19) == ' '
      case (3, 21) => board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (3, 22) => board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '

      case (4, 16) => board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(17) == ' ' && board(18) == ' '
      case (4, 17) => board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(18) == ' '
      case (4, 18) => board(5) == ' ' && board(6) == ' ' && board(7) == ' '
      case (4, 19) => board(5) == ' ' && board(6) == ' ' && board(7) == ' '
      case (4, 20) => board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' '
      case (4, 21) => board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' '
      case (4, 22) => board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (5, 16) => board(6) == ' ' && board(7) == ' ' && board(17) == ' ' && board(18) == ' '
      case (5, 17) => board(6) == ' ' && board(7) == ' ' && board(18) == ' '
      case (5, 18) => board(6) == ' ' && board(7) == ' '
      case (5, 19) => board(6) == ' ' && board(7) == ' '
      case (5, 20) => board(6) == ' ' && board(7) == ' ' && board(19) == ' '
      case (5, 21) => board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' '
      case (5, 22) => board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (6, 16) => board(7) == ' ' && board(17) == ' ' && board(18) == ' '
      case (6, 17) => board(7) == ' ' && board(18) == ' '
      case (6, 18) => board(7) == ' '
      case (6, 19) => board(7) == ' '
      case (6, 20) => board(7) == ' ' && board(19) == ' '
      case (6, 21) => board(7) == ' ' && board(19) == ' ' && board(20) == ' '
      case (6, 22) => board(7) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (7, 16) => board(17) == ' ' && board(18) == ' '
      case (7, 17) => board(18) == ' '
      case (7, 18) => true
      case (7, 19) => true
      case (7, 20) => board(19) == ' '
      case (7, 21) => board(19) == ' ' && board(20) == ' '
      case (7, 22) => board(19) == ' ' && board(20) == ' ' && board(21) == ' '

      case (8, 16)  => board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (8, 17)  => board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(18) == ' ' && board(19) == ' '
      case (8, 18)  => board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(19) == ' '
      case (8, 19)  => board(9) == ' ' && board(10) == ' ' && board(11) == ' '
      case (8, 20)  => board(9) == ' ' && board(10) == ' ' && board(11) == ' '
      case (8, 21)  => board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(20) == ' '
      case (8, 22)  => board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(20) == ' ' && board(21) == ' '
      case (9, 16)  => board(10) == ' ' && board(11) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (9, 17)  => board(10) == ' ' && board(11) == ' ' && board(18) == ' ' && board(19) == ' '
      case (9, 18)  => board(10) == ' ' && board(11) == ' ' && board(19) == ' '
      case (9, 19)  => board(10) == ' ' && board(11) == ' '
      case (9, 20)  => board(10) == ' ' && board(11) == ' '
      case (9, 21)  => board(10) == ' ' && board(11) == ' ' && board(20) == ' '
      case (9, 22)  => board(10) == ' ' && board(11) == ' ' && board(20) == ' ' && board(21) == ' '
      case (10, 16) => board(11) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (10, 17) => board(11) == ' ' && board(18) == ' ' && board(19) == ' '
      case (10, 18) => board(11) == ' ' && board(19) == ' '
      case (10, 19) => board(11) == ' '
      case (10, 20) => board(11) == ' '
      case (10, 21) => board(11) == ' ' && board(20) == ' '
      case (10, 22) => board(11) == ' ' && board(20) == ' ' && board(21) == ' '
      case (11, 16) => board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (11, 17) => board(18) == ' ' && board(19) == ' '
      case (11, 18) => board(19) == ' '
      case (11, 19) => true
      case (11, 20) => true
      case (11, 21) => board(20) == ' '
      case (11, 22) => board(20) == ' ' && board(21) == ' '

      case (12, 16) => board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (12, 17) => board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (12, 18) => board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(19) == ' ' && board(20) == ' '
      case (12, 19) => board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(20) == ' '
      case (12, 20) => board(13) == ' ' && board(14) == ' ' && board(15) == ' '
      case (12, 21) => board(13) == ' ' && board(14) == ' ' && board(15) == ' '
      case (12, 22) => board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(21) == ' '
      case (13, 16) => board(14) == ' ' && board(15) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (13, 17) => board(14) == ' ' && board(15) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (13, 18) => board(14) == ' ' && board(15) == ' ' && board(19) == ' ' && board(20) == ' '
      case (13, 19) => board(14) == ' ' && board(15) == ' ' && board(20) == ' '
      case (13, 20) => board(14) == ' ' && board(15) == ' '
      case (13, 21) => board(14) == ' ' && board(15) == ' '
      case (13, 22) => board(14) == ' ' && board(15) == ' ' && board(21) == ' '
      case (14, 16) => board(15) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (14, 17) => board(15) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (14, 18) => board(15) == ' ' && board(19) == ' ' && board(20) == ' '
      case (14, 19) => board(15) == ' ' && board(20) == ' '
      case (14, 20) => board(15) == ' '
      case (14, 21) => board(15) == ' '
      case (14, 22) => board(15) == ' ' && board(21) == ' '
      case (15, 16) => board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (15, 17) => board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (15, 18) => board(19) == ' ' && board(20) == ' '
      case (15, 19) => board(20) == ' '
      case (15, 20) => true
      case (15, 21) => true
      case (15, 22) => board(21) == ' '

      case (16, 0) => board(16) == 'A' && board(0) == ' ' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(17) == ' '
      case (16, 1) => board(16) == 'A' && board(0) == 'A' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(17) == ' '
      case (16, 2) => board(16) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == ' ' && board(3) == ' ' && board(17) == ' '
      case (16, 3) => board(16) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == 'A' && board(3) == ' ' && board(17) == ' '
      case (17, 0) => board(17) == 'A' && board(0) == ' ' && board(1) == ' ' && board(2) == ' ' && board(3) == ' '
      case (17, 1) => board(17) == 'A' && board(0) == 'A' && board(1) == ' ' && board(2) == ' ' && board(3) == ' '
      case (17, 2) => board(17) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == ' ' && board(3) == ' '
      case (17, 3) => board(17) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == 'A' && board(3) == ' '
      case (18, 0) => board(18) == 'A' && board(0) == ' ' && board(1) == ' ' && board(2) == ' ' && board(3) == ' '
      case (18, 1) => board(18) == 'A' && board(0) == 'A' && board(1) == ' ' && board(2) == ' ' && board(3) == ' '
      case (18, 2) => board(18) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == ' ' && board(3) == ' '
      case (18, 3) => board(18) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == 'A' && board(3) == ' '
      case (19, 0) => board(19) == 'A' && board(0) == ' ' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' '
      case (19, 1) => board(19) == 'A' && board(0) == 'A' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' '
      case (19, 2) => board(19) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == ' ' && board(3) == ' ' && board(18) == ' '
      case (19, 3) => board(19) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == 'A' && board(3) == ' ' && board(18) == ' '
      case (20, 0) => board(20) == 'A' && board(0) == ' ' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' '
      case (20, 1) => board(20) == 'A' && board(0) == 'A' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' '
      case (20, 2) => board(20) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' '
      case (20, 3) => board(20) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == 'A' && board(3) == ' ' && board(18) == ' ' && board(19) == ' '
      case (21, 0) => board(21) == 'A' && board(0) == ' ' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (21, 1) => board(21) == 'A' && board(0) == 'A' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (21, 2) => board(21) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (21, 3) => board(21) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == 'A' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (22, 0) => board(22) == 'A' && board(0) == ' ' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 1) => board(22) == 'A' && board(0) == 'A' && board(1) == ' ' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 2) => board(22) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == ' ' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 3) => board(22) == 'A' && board(0) == 'A' && board(1) == 'A' && board(2) == 'A' && board(3) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '

      case (16, 4) => board(16) == 'B' && board(4) == ' ' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(18) == ' ' && board(17) == ' '
      case (16, 5) => board(16) == 'B' && board(4) == 'B' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(18) == ' ' && board(17) == ' '
      case (16, 6) => board(16) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == ' ' && board(7) == ' ' && board(18) == ' ' && board(17) == ' '
      case (16, 7) => board(16) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == 'B' && board(7) == ' ' && board(18) == ' ' && board(17) == ' '
      case (17, 4) => board(17) == 'B' && board(4) == ' ' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(18) == ' '
      case (17, 5) => board(17) == 'B' && board(4) == 'B' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(18) == ' '
      case (17, 6) => board(17) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == ' ' && board(7) == ' ' && board(18) == ' '
      case (17, 7) => board(17) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == 'B' && board(7) == ' ' && board(18) == ' '
      case (18, 4) => board(18) == 'B' && board(4) == ' ' && board(5) == ' ' && board(6) == ' ' && board(7) == ' '
      case (18, 5) => board(18) == 'B' && board(4) == 'B' && board(5) == ' ' && board(6) == ' ' && board(7) == ' '
      case (18, 6) => board(18) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == ' ' && board(7) == ' '
      case (18, 7) => board(18) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == 'B' && board(7) == ' '
      case (19, 4) => board(19) == 'B' && board(4) == ' ' && board(5) == ' ' && board(6) == ' ' && board(7) == ' '
      case (19, 5) => board(19) == 'B' && board(4) == 'B' && board(5) == ' ' && board(6) == ' ' && board(7) == ' '
      case (19, 6) => board(19) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == ' ' && board(7) == ' '
      case (19, 7) => board(19) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == 'B' && board(7) == ' '
      case (20, 4) => board(20) == 'B' && board(4) == ' ' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' '
      case (20, 5) => board(20) == 'B' && board(4) == 'B' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' '
      case (20, 6) => board(20) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == ' ' && board(7) == ' ' && board(19) == ' '
      case (20, 7) => board(20) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == 'B' && board(7) == ' ' && board(19) == ' '
      case (21, 4) => board(21) == 'B' && board(4) == ' ' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' '
      case (21, 5) => board(21) == 'B' && board(4) == 'B' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' '
      case (21, 6) => board(21) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' '
      case (21, 7) => board(21) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == 'B' && board(7) == ' ' && board(19) == ' ' && board(20) == ' '
      case (22, 4) => board(22) == 'B' && board(4) == ' ' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 5) => board(22) == 'B' && board(4) == 'B' && board(5) == ' ' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 6) => board(22) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == ' ' && board(7) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 7) => board(22) == 'B' && board(4) == 'B' && board(5) == 'B' && board(6) == 'B' && board(7) == ' ' && board(19) == ' ' && board(20) == ' ' && board(21) == ' '

      case (16, 8)  => board(16) == 'C' && board(8) == ' ' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (16, 9)  => board(16) == 'C' && board(8) == 'C' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (16, 10) => board(16) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == ' ' && board(11) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (16, 11) => board(16) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == 'C' && board(11) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' '
      case (17, 8)  => board(17) == 'C' && board(8) == ' ' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(18) == ' ' && board(19) == ' '
      case (17, 9)  => board(17) == 'C' && board(8) == 'C' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(18) == ' ' && board(19) == ' '
      case (17, 10) => board(17) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == ' ' && board(11) == ' ' && board(18) == ' ' && board(19) == ' '
      case (17, 11) => board(17) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == 'C' && board(11) == ' ' && board(18) == ' ' && board(19) == ' '
      case (18, 8)  => board(18) == 'C' && board(8) == ' ' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(19) == ' '
      case (18, 9)  => board(18) == 'C' && board(8) == 'C' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(19) == ' '
      case (18, 10) => board(18) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == ' ' && board(11) == ' ' && board(19) == ' '
      case (18, 11) => board(18) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == 'C' && board(11) == ' ' && board(19) == ' '
      case (19, 8)  => board(19) == 'C' && board(8) == ' ' && board(9) == ' ' && board(10) == ' ' && board(11) == ' '
      case (19, 9)  => board(19) == 'C' && board(8) == 'C' && board(9) == ' ' && board(10) == ' ' && board(11) == ' '
      case (19, 10) => board(19) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == ' ' && board(11) == ' '
      case (19, 11) => board(19) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == 'C' && board(11) == ' '
      case (20, 8)  => board(20) == 'C' && board(8) == ' ' && board(9) == ' ' && board(10) == ' ' && board(11) == ' '
      case (20, 9)  => board(20) == 'C' && board(8) == 'C' && board(9) == ' ' && board(10) == ' ' && board(11) == ' '
      case (20, 10) => board(20) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == ' ' && board(11) == ' '
      case (20, 11) => board(20) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == 'C' && board(11) == ' '
      case (21, 8)  => board(21) == 'C' && board(8) == ' ' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(20) == ' '
      case (21, 9)  => board(21) == 'C' && board(8) == 'C' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(20) == ' '
      case (21, 10) => board(21) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == ' ' && board(11) == ' ' && board(20) == ' '
      case (21, 11) => board(21) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == 'C' && board(11) == ' ' && board(20) == ' '
      case (22, 8)  => board(22) == 'C' && board(8) == ' ' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 9)  => board(22) == 'C' && board(8) == 'C' && board(9) == ' ' && board(10) == ' ' && board(11) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 10) => board(22) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == ' ' && board(11) == ' ' && board(20) == ' ' && board(21) == ' '
      case (22, 11) => board(22) == 'C' && board(8) == 'C' && board(9) == 'C' && board(10) == 'C' && board(11) == ' ' && board(20) == ' ' && board(21) == ' '

      case (16, 12) => board(16) == 'D' && board(12) == ' ' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (16, 13) => board(16) == 'D' && board(12) == 'D' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (16, 14) => board(16) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == ' ' && board(15) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (16, 15) => board(16) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == 'D' && board(15) == ' ' && board(17) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (17, 12) => board(17) == 'D' && board(12) == ' ' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (17, 13) => board(17) == 'D' && board(12) == 'D' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (17, 14) => board(17) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == ' ' && board(15) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (17, 15) => board(17) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == 'D' && board(15) == ' ' && board(18) == ' ' && board(19) == ' ' && board(20) == ' '
      case (18, 12) => board(18) == 'D' && board(12) == ' ' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(19) == ' ' && board(20) == ' '
      case (18, 13) => board(18) == 'D' && board(12) == 'D' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(19) == ' ' && board(20) == ' '
      case (18, 14) => board(18) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == ' ' && board(15) == ' ' && board(19) == ' ' && board(20) == ' '
      case (18, 15) => board(18) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == 'D' && board(15) == ' ' && board(19) == ' ' && board(20) == ' '
      case (19, 12) => board(19) == 'D' && board(12) == ' ' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(20) == ' '
      case (19, 13) => board(19) == 'D' && board(12) == 'D' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(20) == ' '
      case (19, 14) => board(19) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == ' ' && board(15) == ' ' && board(20) == ' '
      case (19, 15) => board(19) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == 'D' && board(15) == ' ' && board(20) == ' '
      case (20, 12) => board(20) == 'D' && board(12) == ' ' && board(13) == ' ' && board(14) == ' ' && board(15) == ' '
      case (20, 13) => board(20) == 'D' && board(12) == 'D' && board(13) == ' ' && board(14) == ' ' && board(15) == ' '
      case (20, 14) => board(20) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == ' ' && board(15) == ' '
      case (20, 15) => board(20) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == 'D' && board(15) == ' '
      case (21, 12) => board(21) == 'D' && board(12) == ' ' && board(13) == ' ' && board(14) == ' ' && board(15) == ' '
      case (21, 13) => board(21) == 'D' && board(12) == 'D' && board(13) == ' ' && board(14) == ' ' && board(15) == ' '
      case (21, 14) => board(21) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == ' ' && board(15) == ' '
      case (21, 15) => board(21) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == 'D' && board(15) == ' '
      case (22, 12) => board(22) == 'D' && board(12) == ' ' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(21) == ' '
      case (22, 13) => board(22) == 'D' && board(12) == 'D' && board(13) == ' ' && board(14) == ' ' && board(15) == ' ' && board(21) == ' '
      case (22, 14) => board(22) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == ' ' && board(15) == ' ' && board(21) == ' '
      case (22, 15) => board(22) == 'D' && board(12) == 'D' && board(13) == 'D' && board(14) == 'D' && board(15) == ' ' && board(21) == ' '

      case (_, _) => false
    }
  }

  def freeSpots(board: String): List[Int] = {
    board.zipWithIndex.filter(_._1 == ' ').map(_._2).toList
  }

  def movableAmphipods(board: String): List[Int] = {
    val positions = board.zipWithIndex.filter(_._1 != ' ').map(_._2)
    positions
      .map {
        case 0  => (0, !positions.contains(1) && !positions.contains(2) && !positions.contains(3))
        case 1  => (1, !positions.contains(2) && !positions.contains(3))
        case 2  => (2, !positions.contains(3))
        case 4  => (4, !positions.contains(5) && !positions.contains(6) && !positions.contains(7))
        case 5  => (5, !positions.contains(6) && !positions.contains(7))
        case 6  => (6, !positions.contains(7))
        case 8  => (8, !positions.contains(9) && !positions.contains(10) && !positions.contains(11))
        case 9  => (9, !positions.contains(10) && !positions.contains(11))
        case 10 => (10, !positions.contains(11))
        case 12 => (12, !positions.contains(13) && !positions.contains(14) && !positions.contains(15))
        case 13 => (13, !positions.contains(14) && !positions.contains(15))
        case 14 => (14, !positions.contains(15))
        case 16 => (16, !positions.contains(17))
        case 22 => (22, !positions.contains(21))
        case p  => (p, true)
      }
      .filter(_._2 == true)
      .map(_._1)
      .toList
  }

  def move(board: String, move: Move): String = {
    val temp: Array[Char] = board.toCharArray
    temp(move.to) = board(move.from)
    temp(move.from) = ' '
    if (temp.count(_ == 'A') != 4 || temp.count(_ == 'B') != 4 || temp.count(_ == 'C') != 4 || temp.count(_ == 'D') != 4) {
      println(s"Warning $board move $move gives invalid result: ${temp.mkString}")
    }
    temp.mkString
  }
}

object Day23 extends AOCApp {
  val lines = getInput("aoc2021_23_part2.txt")
  //val lines = getInput("aoc2021_23_part1_large.txt")

  println("Manually calculated, part 1 19019, part 2 47533")

  val startPos = BurrowBoard.fromInput(lines)
  BurrowBoard.showBoard(startPos)

  val knownBoards = mutable.Map[String, (Int, List[Move])]()

  // push the start position as a known board
  knownBoards(startPos) = (0, List.empty)

  val boardQueue = mutable.PriorityQueue[(Int, String)]().reverse

  // add the start position to the queue
  boardQueue.enqueue((0, startPos))

  val maxSteps = Int.MaxValue
  //val maxSteps = 0
  var counter = 0
  var loopStart = System.nanoTime()
  var lowestFinish = 50000 // known upper bound for the score

  while (boardQueue.nonEmpty && counter < maxSteps) {
    counter += 1
    val (score, board) = boardQueue.dequeue()

    // if current score is the best known score, we consider all possible moves
    if (knownBoards(board)._1 == score) {
      val journey = knownBoards(board)._2

      val moves = BurrowBoard.findPossibleMoves(board)
      moves.foreach(m => {
        if (m.cost + score < lowestFinish) {
          val newBoard = BurrowBoard.move(board, m)
          // keep the new board if it is cheaper than the current known board (if any)
          if (!knownBoards.contains(newBoard) || knownBoards(newBoard)._1 > m.cost + score) {
            knownBoards(newBoard) = (m.cost + score, journey :+ m)
            boardQueue.enqueue((m.cost + score, newBoard))
          }
          if (BurrowBoard.isFinish(newBoard)) {
            println(s"We have a finish at ${m.cost + score}")
            (journey :+ m).foreach(println)
            lowestFinish = m.cost + score
          }
        }
      })
    }

    if (counter % 1000 == 0 && System.nanoTime() - loopStart > 10e8) {
      loopStart = System.nanoTime()
      println(
        s"$counter iterations done, queue is now ${boardQueue.size}, we have ${knownBoards.size} known boards, lowest finish is at $lowestFinish"
      )
    }
  }

  println(s"Queue is empty, lowestFinish is $lowestFinish")

}
