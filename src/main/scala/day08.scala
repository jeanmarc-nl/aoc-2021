package nl.about42.aoc2021

object day08 extends AOCApp {
  val lines = getInputForDay("08", true)

  val data = lines.map(parse)

  val simpleOutput = data.map(_(1)).map(_.trim.split(' ').toList.map(_.length))

  val digitCount =
    simpleOutput
      .map(_.count(d => d == 2 || d == 3 || d == 4 || d == 7))
      .sum
  println(s"NUmber of 1/4/7/8 digits: $digitCount")

  val readout =
    data.map(line =>
      (
        line(0).split(' ').map(_.sorted).toList,
        line(1).split(' ').map(_.sorted).toList
      )
    )

  var sum = 0
  readout.foreach(line => {
    val digits = assignDigits(line._1).toMap
    val readout = line._2.map(s => digits(s)).mkString.toInt
    sum += readout
  })
  println(s"Total sum of readouts: $sum")

  def assignDigits(in: List[String]): List[(String, Int)] = {
    val one = in.find(_.length == 2).get
    val four = in.find(_.length == 4).get
    val seven = in.find(_.length == 3).get

    val data = in.map(e => (e, e.length))

    // digits with 6 elements can be determined by checking if they overlap with four or seven
    // overlap with four -> it must be 9
    // overlap with seven -> it must be 0
    // else it must be 6
    val partial = data.map(e =>
      (
        e._1,
        e._2, {
          e._2 match {
            case 7 => 8
            case 2 => 1
            case 4 => 4
            case 3 => 7
            case 6 =>
              if (isPart(four, e._1)) 9
              else if (isPart(seven, e._1)) 0
              else 6
            case _ =>
              0 // 5 segments, correct number will be determined in next phase
          }
        }
      )
    )
    // extract the string that represents the digit 6
    val six = partial.find(p => p._3 == 6).map(_._1).get

    // digits with 5 elements can be determined by checking if they overlap with one or six
    // overlap with one -> it must be 3
    // overlap with six -> it must be 5
    // else it must be 2
    partial.map(e =>
      (
        e._1, {
          e._2 match {
            case 5 =>
              if (isPart(one, e._1)) 3
              else if (isPart(e._1, six)) 5
              else 2
            case _ => e._3
          }
        }
      )
    )
  }

  def isPart(a: String, b: String): Boolean = {
    // true if all characters in a occur in b
    a.foreach(c => {
      if (!b.contains(c)) return false
    })
    true
  }

  def parse(in: String) = {
    in.split('|').map(_.trim).toList
  }
}
