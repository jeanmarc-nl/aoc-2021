package nl.about42.aoc2021
package graph

case class Vertex(id: String)
case class Edge(v1: Vertex, v2: Vertex, weight: Int = 1)

trait Graph {
  def addEdge(e: Edge): Graph
  def addVertex(v: Vertex): Graph
}

class UndiGraph(val neighbors: Map[Vertex, Set[Vertex]]) extends Graph {
  override def addEdge(e: Edge): UndiGraph = {
    if (
      neighbors.contains(e.v1) && neighbors(e.v1).contains(e.v2) &&
      neighbors.contains(e.v2) && neighbors(e.v2).contains(e.v1)
    ) {
      this
    } else {
      val v1set = neighbors.getOrElse(e.v1, Set.empty) + e.v2
      val v2set = neighbors.getOrElse(e.v2, Set.empty) + e.v1
      new UndiGraph(neighbors + (e.v1 -> v1set) + (e.v2 -> v2set))
    }
  }

  override def addVertex(v: Vertex): Graph = {
    if (neighbors.contains(v)) {
      this
    } else {
      new UndiGraph(neighbors + (v -> Set.empty))
    }
  }
}

class DiGraph(val successors: Map[Vertex, Set[Vertex]] = Map.empty)
    extends Graph {
  override def addEdge(e: Edge): DiGraph = {
    if (successors.contains(e.v1) && successors(e.v1).contains(e.v2)) {
      this
    } else {
      val v1set = successors.getOrElse(e.v1, Set.empty) + e.v2
      new DiGraph(successors + (e.v1 -> v1set))
    }
  }

  override def addVertex(v: Vertex): Graph = {
    if (successors.contains(v)) {
      this
    } else {
      new DiGraph(successors + (v -> Set.empty))
    }
  }

}
