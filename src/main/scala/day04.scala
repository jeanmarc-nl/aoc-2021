package nl.about42.aoc2021

case class Card(rows: Vector[Vector[Int]])

class BingoCard(val numbers: Array[Array[(Int, Boolean)]]) {
  def process(n: Int): Boolean = {
    (0 until 5).foreach(row => processLine(n, numbers(row)))
    hasBingo
  }
  def processLine(n: Int, l: Array[(Int, Boolean)]) = {
    (0 until 5).foreach(el => {
      if (l(el)._1 == n) {
        l.update(el, (l(el)._1, true))
      }
    })
  }
  def hasBingo: Boolean = {
    // any row bingo?
    (0 until 5).foreach(row => {
      val score = numbers(row).map(_._2).count(x => x)
      if (score == 5) {
        println(s"Row bingo")
        this.showCard
        return true
      }
    })

    // any col bingo?
    (0 until 5).foreach(i => {
      val score = getCol(i).map(_._2).count(x => x)
      if (score == 5) {
        println(s"Col bingo")
        this.showCard
        return true
      }
    })

    // no bingo
    false
  }

  def getCol(col: Int): Seq[(Int, Boolean)] = {
    numbers.map(row => row(col)).toIndexedSeq
  }

  def getUnmarkedSum: Int = {
    numbers.map(row => row.filter(e => !e._2).map(e => e._1).sum).sum
  }

  def showCard = {
    (0 until 5).foreach(r => {
      (0 until 5).foreach(el => {
        print(f"  ${numbers(r)(el)._1}%2d ")
        if (numbers(r)(el)._2) {
          print("*")
        } else {
          print(" ")
        }
      })
      println()
    })
  }

}

object day04 extends AOCApp {
  //val lines = getInput("04small.txt")
  val lines = getInputForDay("04", true)

  val numbers = lines.head.split(',').map(_.toInt).toList

  val cards = getCards(lines.tail.tail)

  val bc = cards.map(c =>
    new BingoCard(
      c.rows.map(line => line.map(el => (el, false)).toArray).toArray
    )
  )

  var done = false
  var playingCards = bc.toSet
  numbers.foreach(n => {
    if (playingCards.nonEmpty) {
      println(s"Draw was $n there are ${playingCards.size} cards remaining")

      playingCards.foreach(c => {
        if (c.process(n)) {
          showCard(n, c)
          println(s"Bingo for number $n, remove from players")
          playingCards = playingCards - c
        }
      })
    }
  })
  if (playingCards.nonEmpty) {
    println(s"No more cards left.")
  } else {
    println(s"We're out of numbers, we have ${playingCards.size} card(s) left.")
  }

  def showCard(n: Int, c: BingoCard) = {
    val sum = c.getUnmarkedSum
    println(s"Card sum is $sum, last draw was $n, score is ${n * sum}")
  }

  def getCards(in: List[String], c: Seq[Card] = Seq.empty): Seq[Card] = {
    if (in.size < 5) {
      c
    } else {
      val cardLines = getCard(in.take(5))
      getCards(in.drop(6), c :+ cardLines)
    }

  }

  def getCard(in: List[String]): Card = {
    Card(
      in.map(l =>
        l.split("""\s+""").filter(el => el != "").map(_.toInt).toVector
      ).toVector
    )
  }
}
