package nl.about42.aoc2021

import scala.collection.mutable.ListBuffer
import scala.sys.exit
import scala.util.control.Breaks.{break, breakable}

trait Axis
case object PosX extends Axis
case object NegX extends Axis
case object PosY extends Axis
case object NegY extends Axis
case object PosZ extends Axis
case object NegZ extends Axis

case class Orientation(face: Axis, up: Axis) {
  def toGlobal(pos: PositionXYZ): PositionXYZ = {
    (face, up) match {
      case (PosZ, PosX) => PositionXYZ(pos.y * -1, pos.x, pos.z)
      case (PosZ, NegX) => PositionXYZ(pos.y, pos.x * -1, pos.z)
      case (PosZ, PosY) => PositionXYZ(pos.x, pos.y, pos.z)
      case (PosZ, NegY) => PositionXYZ(pos.x * -1, pos.y * -1, pos.z)

      case (NegZ, PosX) => PositionXYZ(pos.y, pos.x, pos.z * -1)
      case (NegZ, NegX) => PositionXYZ(pos.y * -1, pos.x * -1, pos.z * -1)
      case (NegZ, PosY) => PositionXYZ(pos.x * -1, pos.y, pos.z * -1)
      case (NegZ, NegY) => PositionXYZ(pos.x, pos.y * -1, pos.z * -1)

      case (PosX, PosY) => PositionXYZ(pos.z * -1, pos.y, pos.x)
      case (PosX, NegY) => PositionXYZ(pos.z, pos.y * -1, pos.x)
      case (PosX, PosZ) => PositionXYZ(pos.y, pos.z, pos.x)
      case (PosX, NegZ) => PositionXYZ(pos.y * -1, pos.z * -1, pos.x)

      case (NegX, PosY) => PositionXYZ(pos.z, pos.y, pos.x * -1)
      case (NegX, NegY) => PositionXYZ(pos.z * -1, pos.y * -1, pos.x * -1)
      case (NegX, PosZ) => PositionXYZ(pos.y * -1, pos.z, pos.x * -1)
      case (NegX, NegZ) => PositionXYZ(pos.y, pos.z * -1, pos.x * -1)

      case (PosY, PosX) => PositionXYZ(pos.z, pos.x, pos.y)
      case (PosY, NegX) => PositionXYZ(pos.z * -1, pos.x * -1, pos.y)
      case (PosY, PosZ) => PositionXYZ(pos.x * -1, pos.z, pos.y)
      case (PosY, NegZ) => PositionXYZ(pos.x, pos.z * -1, pos.y)

      case (NegY, PosX) => PositionXYZ(pos.z * -1, pos.x, pos.y * -1)
      case (NegY, NegX) => PositionXYZ(pos.z, pos.x * -1, pos.y * -1)
      case (NegY, PosZ) => PositionXYZ(pos.x, pos.z, pos.y * -1)
      case (NegY, NegZ) => PositionXYZ(pos.x * -1, pos.z * -1, pos.y * -1)
    }
  }
}

case object PositionXYZ {
  def add(a: PositionXYZ, b: PositionXYZ) =
    PositionXYZ(b.x + a.x, b.y + a.y, b.z + a.z)
  def deltaBtoA(a: PositionXYZ, b: PositionXYZ) =
    PositionXYZ(a.x - b.x, a.y - b.y, a.z - b.z)

}
case class PositionXYZ(x: Int, y: Int, z: Int)
case class ScannerData(id: Int, positions: List[PositionXYZ])
case class Scanner(
    id: Int,
    position: PositionXYZ,
    origPos: PositionXYZ,
    orientation: Orientation,
    data: ScannerData
) {
  def getPositions: Set[PositionXYZ] = {
    data.positions.map(p => PositionXYZ.add(position, p)).toSet
  }
}
object day19 extends AOCApp {
  val lines = getInputForDay("19", false)
  //val lines = getInput("19small.txt")

  val matchLimit = 12

  val data = ListBuffer[ScannerData]()

  var start = 0
  while (start < lines.length) {
    val (scanData, next) = getScannerData(start)
    data.append(scanData)
    start = next
  }

  val scanData = data
  //scanData.foreach(println)

  val knownScanners = ListBuffer[Scanner]()

  knownScanners.append(
    Scanner(
      scanData.head.id,
      PositionXYZ(0, 0, 0),
      PositionXYZ(0, 0, 0),
      Orientation(PosZ, PosY),
      scanData.head
    )
  )
  scanData.remove(0)

  // beacons can be seen if they are between [-1000..1000] inclusive in any x, y, or z
  // overlap if at least 12 common beacons between 2 scanners

  val orientations = List(
    Orientation(PosZ, PosX),
    Orientation(PosZ, NegX),
    Orientation(PosZ, PosY),
    Orientation(PosZ, NegY),
    Orientation(NegZ, PosX),
    Orientation(NegZ, NegX),
    Orientation(NegZ, PosY),
    Orientation(NegZ, NegY),
    Orientation(PosX, PosY),
    Orientation(PosX, NegY),
    Orientation(PosX, PosZ),
    Orientation(PosX, NegZ),
    Orientation(NegX, PosY),
    Orientation(NegX, NegY),
    Orientation(NegX, PosZ),
    Orientation(NegX, NegZ),
    Orientation(PosY, PosX),
    Orientation(PosY, NegX),
    Orientation(PosY, PosZ),
    Orientation(PosY, NegZ),
    Orientation(NegY, PosX),
    Orientation(NegY, NegX),
    Orientation(NegY, PosZ),
    Orientation(NegY, NegZ)
  )

//  orientations.foreach(o => {
//    println(s"data interpreted as $o:")
//    println(s"${reOrient(scanData(0), o)}")
//  })

  while (scanData.nonEmpty) {
    // look for a match against any of the known scanners, and add it
    val dataSize = scanData.length
    breakable {
      scanData.zipWithIndex.foreach({ case (sd, i) =>
        knownScanners.foreach(known => {
          val matchInfo = hasMatch(known, sd)
          if (matchInfo.nonEmpty) {
            //println(s"New scanner:\n${matchInfo.get}")
            knownScanners.append(matchInfo.get)
            scanData.remove(i)
            break()
          }
        })
      })
    }
    println(
      s"looped, ${scanData.length} remaining, we have ${knownScanners.length} known scanners"
    )
    if (scanData.length == dataSize) {
      println("Problem: no new match found, aborting")
      exit()
    }
  }

  val scanners = knownScanners.toList

  val allPositions = scanners.flatMap(_.getPositions).toSet

  println(s"there are ${allPositions.size} positions")

  // find biggest manhattan distance
  val positions = scanners.map(_.origPos)
  var maxMan = 0
  positions.foreach(p1 =>
    positions.foreach(p2 => {
      if (manhattan(p1, p2) > maxMan) maxMan = manhattan(p1, p2)
    })
  )

  println(s"Max manhattan is $maxMan for\n$positions")

  def manhattan(a: PositionXYZ, b: PositionXYZ): Int = {
    Math.abs(a.x - b.x) + Math.abs(a.y - b.y) + Math.abs(a.z - b.z)
  }

  def hasMatch(
      a: Scanner,
      b: ScannerData
  ): Option[Scanner] = {
    // look for a match of at least 12 points for any orientation
    // return the orientation and the position of scanner b if a match is found
    orientations.foreach(o => {
      val normalizedData = reOrient(b, o)
      // check for each combination of point in a and point in b how many matches there are.
      // If 12 or more we have a match
      a.data.positions.foreach(ap => {
        normalizedData.positions.foreach(bp => {
          val delta = PositionXYZ.deltaBtoA(ap, bp)
          if (matchCount(a.data, normalizedData, delta) >= matchLimit) {
            return Some(
              Scanner(
                b.id,
                PositionXYZ(0, 0, 0),
                delta,
                Orientation(PosZ, PosY),
                ScannerData(
                  normalizedData.id,
                  normalizedData.positions.map(PositionXYZ.add(_, delta))
                )
              )
            )
          }
        })
      })
    })
    None
  }

  def matchCount(a: ScannerData, b: ScannerData, delta: PositionXYZ): Int = {
    var matches = 0
    b.positions.foreach(bp =>
      if (a.positions.contains(PositionXYZ.add(bp, delta))) matches += 1
    )
    matches
  }

  // transform to global as if scanData has the given Orientation
  def reOrient(scanData: ScannerData, orientation: Orientation): ScannerData = {
    val coords = scanData.positions.map(p => orientation.toGlobal(p))
    ScannerData(scanData.id, coords)
  }

  def getScannerData(start: Int): (ScannerData, Int) = {
    val id = lines(start).substring(12).split(' ').head.toInt
    val coords = ListBuffer[PositionXYZ]()
    (start + 1 until lines.length).foreach(i => {
      if (lines(i).isEmpty) {
        return (ScannerData(id, coords.toList), i + 1)
      }
      val data = lines(i).split(',').map(_.toInt)
      coords.append(PositionXYZ(data(0), data(1), data(2)))
    })
    (ScannerData(id, coords.toList), lines.length)
  }
}
