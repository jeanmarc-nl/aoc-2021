package nl.about42.aoc2021
package scoredata

case class Board(
    event: String,
    owner_id: String,
    members: Map[String, Member]
) {
  val startTS = event match {
    case "2021" => 1638334800 // 2021-12-01
    case "2020" => 1606795200 // 2020-12-01
    case _      => 1291176000 // 2010-12-01
  }
}

case class Member(
    id: String,
    last_star_ts: Long,
    local_score: Int,
    completion_day_level: Map[String, Map[String, Star]],
    stars: Int,
    global_score: Int,
    name: Option[String]
)

case class Star(get_star_ts: Long)

case class DayScore(
    id: String,
    name: String,
    day: Int,
    rank1: Int,
    time1: Long,
    rank2: Option[Int],
    time2: Option[Long]
) {
  def getStar1Score(numContestants: Int): Int = numContestants + 1 - rank1
  def getOverallScore(numContestants: Int): Int =
    numContestants + 1 - rank1 + rank2.map(numContestants + 1 - _).getOrElse(0)
  def getLastStar: Long = time2.getOrElse(time1)
}
