package nl.about42.aoc2021

case class NumberPart(ord: Long, level: Int, side: String)

object day18 extends AOCApp {
  val lines = getInputForDay("18", true)
  //val lines = getInput("18small.txt")

  val n1 = parseSnailNumber(lines(0))

  val numbers = lines.map(parseSnailNumber(_))

  println(s"got ${numbers.size} numbers")

  val sum = addNumbers(numbers)

  println("total")
  showNumber(sum)

  println(s"total magnitude is ${calcMagnitude(sum)}")

  // find biggest from any 2
  var max = 0L

  numbers.zipWithIndex.foreach { case (n, i) =>
    (i + 1 until numbers.size - 1).foreach { case j =>
      val magn = calcMagnitude(addNumbers(List(numbers(i), numbers(j))))
      if (magn.ord > max) max = magn.ord
      val magn2 = calcMagnitude(addNumbers(List(numbers(j), numbers(i))))
      if (magn2.ord > max) max = magn2.ord
    }
  }

  println(s"Max is $max")

  def calcMagnitude(numbers: List[NumberPart]): NumberPart = {
    // find pairs of L/R of highest level and combine them to 1 number of level - 1
    // continue until left with a single number
    var done = false
    var workList = numbers
    var index = 0
    def isPairOfLevel(i: Int, level: Int): Boolean = {
      workList(i).level == level &&
      workList(i + 1).level == level &&
      workList(i).side.head == 'L' &&
      workList(i + 1).side.head == 'R'
    }
    while (!done) {
      if (workList.length == 1)
        return workList.head

      val maxLevel = workList.map(n => n.level).max
      index = 0
      while (!isPairOfLevel(index, maxLevel)) {
        index += 1
      }
      val leftSide = workList.take(index)
      val rightSide = workList.takeRight(workList.size - 2 - index)
      workList = leftSide ++
        List(magnitude(workList(index), workList(index + 1))) ++
        rightSide
    }
    workList.head
  }

  def magnitude(a: NumberPart, b: NumberPart): NumberPart = {
    if (a.level == b.level && a.side.head == 'L' && b.side.head == 'R') {
      NumberPart(3 * a.ord + 2 * b.ord, a.level - 1, a.side.tail)
    } else {
      println(s"warning, wrong levels or sides $a - $b")
      NumberPart(0, a.level, a.side.tail)
    }
  }

  def addNumbers(numbers: List[List[NumberPart]]): List[NumberPart] = {
    numbers match {
      case n1 :: n2 :: tail =>
        val sum = add(n1, n2)
        addNumbers(sum +: tail)
      case n :: nil => n
      case nil      => List.empty
    }
  }

  def add(n1: List[NumberPart], n2: List[NumberPart]): List[NumberPart] = {
    reduce(
      n1.map(np => NumberPart(np.ord, np.level + 1, np.side + "L")) ++
        n2.map(np => NumberPart(np.ord, np.level + 1, np.side + "R"))
    )
  }

  def reduce(num: List[NumberPart]): List[NumberPart] = {
    var done = false
    var res = num
    while (!done) {
      val (explRes, explDone) = explode(res)
      if (!explDone) {
        val (splitRes, splitDone) = split(res)
        if (!splitDone) {
          done = true
        } else {
          res = splitRes
        }
      } else {
        res = explRes
      }
    }
    res
  }

  def explode(num: List[NumberPart]): (List[NumberPart], Boolean) = {
    val firstExplode = num.indexWhere(np => np.level == 5)
    if (firstExplode >= 0) {
      val toExplodeLeft = num(firstExplode)
      val toExplodeRight = num(firstExplode + 1)
      val left = num.take(firstExplode)
      val right = num.takeRight(num.size - 2 - firstExplode)
      val newLeft =
        if (left.isEmpty) left
        else
          left.dropRight(1) :+ NumberPart(
            left.last.ord + toExplodeLeft.ord,
            left.last.level,
            left.last.side
          )
      val newRight =
        if (right.isEmpty) right
        else {
          NumberPart(
            right.head.ord + toExplodeRight.ord,
            right.head.level,
            right.head.side
          ) +: right.tail
        }

      if (toExplodeLeft.side.tail != toExplodeRight.side.tail) {
        println(
          s"warning - unexpected combination $toExplodeLeft and $toExplodeRight"
        )
      }
      (
        newLeft ++ List(NumberPart(0, 4, toExplodeLeft.side.tail)) ++ newRight,
        true
      )
    } else {
      (num, false)
    }
  }

  def split(num: List[NumberPart]): (List[NumberPart], Boolean) = {
    val firstSplit = num.indexWhere(np => np.ord >= 10)
    if (firstSplit >= 0) {
      val toSplit = num(firstSplit)
      (
        num.take(firstSplit) ++ List(
          NumberPart(toSplit.ord / 2, toSplit.level + 1, "L" + toSplit.side),
          NumberPart(
            toSplit.ord - (toSplit.ord / 2),
            toSplit.level + 1,
            "R" + toSplit.side
          )
        ) ++ num.takeRight(num.size - 1 - firstSplit),
        true
      )
    } else {
      (num, false)
    }
  }

  def parseSnailNumber(
      in: String,
      level: Int = 0,
      side: String = "",
      acc: List[NumberPart] = List.empty
  ): List[NumberPart] = {
    if (in.isEmpty) {
      return acc
    }

    in(0) match {
      case '[' => // level up
        parseSnailNumber(in.tail, level + 1, "L" + side, acc)
      case ']' => // level down
        parseSnailNumber(in.tail, level - 1, side.tail, acc)
      case ',' => // left done
        parseSnailNumber(in.tail, level, "R" + side.tail, acc)
      case n => // get a number
        val number = NumberPart(n.asDigit, level, side)
        parseSnailNumber(in.tail, level, side, acc :+ number)
    }
  }

  def showNumber(n: List[NumberPart]) = {
    n.foreach(np => print(f"${np.ord}%3d"))
    println()
    n.foreach(np => print(f"${np.level}%3d"))
    println()
    n.foreach(np => print(s"  ${np.side.head}"))
    println()
  }
}
