package nl.about42.aoc2021

object day15 extends AOCApp {
//  val lines = getInput("15tiny.txt")
//  val lines = getInput("15small.txt")
  val lines = getInputForDay("15", true)

  val plan = intGrid(lines)

  val maxY = plan.length
  val maxX = plan(0).length
  val visited = Array.ofDim[Boolean](plan.length, plan(0).length)
  val distance = Array.fill[Int](plan.length, plan(0).length)(Int.MaxValue)
  distance(0)(0) = 0

  var minRisk: Int = Int.MaxValue

  var currentNode = (0, 0)

  while (currentNode._1 != maxX - 1 || currentNode._2 != maxY - 1) {
    process(currentNode)
    findShortestUnvisited() match {
      case None    => // we are done
      case Some(c) => currentNode = c
    }
  }

  println(s"The distance to end is ${distance(maxX - 1)(maxY - 1)}")

  def findShortestUnvisited(): Option[(Int, Int)] = {
    var currentMin = Int.MaxValue
    var coords = (0, 0)
    distance.indices.foreach(i =>
      distance(0).indices.foreach(j => {
        if (!visited(i)(j)) {
          if (distance(i)(j) < currentMin) {
            currentMin = distance(i)(j)
            coords = (i, j)
          }
        }
      })
    )
    if (currentMin < Int.MaxValue) {
      Some(coords)
    } else { None }
  }

  def process(start: (Int, Int)): Unit = {
    val possibles =
      getNeighbors(start._1, start._2).filter(n => !visited(n._1)(n._2))
    possibles.foreach(next => {
      val riskLevel = distance(start._1)(start._2) + plan(next._1)(next._2)
      distance(next._1)(next._2) =
        Math.min(distance(next._1)(next._2), riskLevel)
    })
    visited(start._1)(start._2) = true
  }

  def getNeighbors(x: Int, y: Int): List[(Int, Int)] = {
    List((x, y + 1), (x, y - 1), (x - 1, y), (x + 1, y)).filter(e =>
      e._1 >= 0 && e._1 < maxX && e._2 >= 0 && e._2 < maxY
    )
  }

}

object day15_2 extends AOCApp {
  //  val lines = getInput("15tiny.txt")
  //val lines = getInput("15small.txt")
  val lines = getInput("aoc2021_15_input.txt")

  val smallPlan = intGrid(lines)
  //showGrid(smallPlan)
  val plan = Array.ofDim[Int](smallPlan.length * 5, smallPlan(0).length * 5)
  val maxY = plan.length
  val maxX = plan(0).length

  // fill the plan
  (0 until 5).foreach(i =>
    (0 until 5).foreach(j => {
      smallPlan.indices.foreach(x =>
        smallPlan(0).indices.foreach(y => {
          val riskLevel = ((smallPlan(x)(y) + i + j) - 1) % 9 + 1
          plan(smallPlan.length * i + x)(smallPlan(0).length * j + y) =
            riskLevel
        })
      )
    })
  )

  //showGrid(plan)

  val visited = Array.ofDim[Boolean](plan.length, plan(0).length)
  val distance = Array.fill[Int](plan.length, plan(0).length)(Int.MaxValue)
  distance(0)(0) = 0

  var minRisk: Int = Int.MaxValue

  var currentNode = (0, 0)

  var done = false
  while (!done) {
    process(currentNode)
    findShortestUnvisited() match {
      case None => done = true // we are done
      case Some(c) =>
        if (currentNode._1 == maxX - 1 && currentNode._2 == maxY - 1)
          done = true
        else currentNode = c
    }
  }

  println(s"The distance to end is ${distance(maxX - 1)(maxY - 1)}")

  def findShortestUnvisited(): Option[(Int, Int)] = {
    var currentMin = Int.MaxValue
    var coords = (0, 0)
    distance.indices.foreach(i =>
      distance(0).indices.foreach(j => {
        if (!visited(i)(j)) {
          if (distance(i)(j) < currentMin) {
            currentMin = distance(i)(j)
            coords = (i, j)
          }
        }
      })
    )
    if (currentMin < Int.MaxValue) {
      Some(coords)
    } else { None }
  }

  def process(start: (Int, Int)): Unit = {
    val possibles =
      getNeighbors(start._1, start._2).filter(n => !visited(n._1)(n._2))
    possibles.foreach(next => {
      val riskLevel = distance(start._1)(start._2) + plan(next._1)(next._2)
      distance(next._1)(next._2) =
        Math.min(distance(next._1)(next._2), riskLevel)
    })
    visited(start._1)(start._2) = true
  }

  def getNeighbors(x: Int, y: Int): List[(Int, Int)] = {
    List((x, y + 1), (x, y - 1), (x - 1, y), (x + 1, y)).filter(e =>
      e._1 >= 0 && e._1 < maxX && e._2 >= 0 && e._2 < maxY
    )
  }

}
