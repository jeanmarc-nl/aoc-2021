package nl.about42.aoc2021

import scala.collection.mutable.ListBuffer

object day09 extends AOCApp {
  //val lines = getInput("09small.txt")
  val lines = getInputForDay("09", true)

  val data = lines.map(_.toArray.map(_.asDigit)).toArray

  val hasCounted = ListBuffer[(Int, Int)]()

  var riskSum = 0
  var basinSizes = ListBuffer[Int]()

  val maxY = lines.head.length
  val maxX = lines.length

  (0 until maxX).foreach(x => {
    (0 until maxY).foreach(y => {
      if (isLower(x, y)) {
        riskSum += riskLevel(x, y)
        val size = getBasinSize(x, y)
        basinSizes += size
      }
    })
  })
  println(s"Part1: The sum of the risk levels of all low points: $riskSum")
  val top3 = basinSizes.toList.sorted.takeRight(3)

  println(
    s"Part2: The biggest basins top 3 product for ${top3}: ${top3.product}"
  )

  def isLower(x: Int, y: Int): Boolean = {

    getNeighbors(x, y).foreach(n =>
      if (data(x)(y) >= data(n._1)(n._2)) {
        return false
      }
    )
    true
  }

  def riskLevel(x: Int, y: Int): Int = {
    data(x)(y) + 1
  }

  def getNeighbors(x: Int, y: Int): List[(Int, Int)] = {
    List((x, y + 1), (x, y - 1), (x - 1, y), (x + 1, y)).filter(e =>
      e._1 >= 0 && e._1 < maxX && e._2 >= 0 && e._2 < maxY
    )
  }

  def getBasinSize(x: Int, y: Int): Int = {
    hasCounted.clear()
    basinSize(x, y)
  }

  def basinSize(x: Int, y: Int): Int = {
    if (hasCounted.contains((x, y))) {
      return 0
    }
    hasCounted.append((x, y))
    1 + getNeighbors(x, y)
      .filter(n => data(n._1)(n._2) < 9 && data(n._1)(n._2) > data(x)(y))
      .map(n => basinSize(n._1, n._2))
      .sum
  }
}
