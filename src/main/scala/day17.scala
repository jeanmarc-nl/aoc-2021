package nl.about42.aoc2021

object day17 extends AOCApp {
  val lines = getInputForDay("17", true)
  //val lines = getInput("17small.txt")

  println(s"${lines(0)}")

  val limits = lines(0).split("[\\s:=.,]")

  // grab the numbers from the input, sort X ascending, sort Y descending (Y is negative)
  val (targetX1, targetX2) =
    List(limits(4).toInt, limits(6).toInt).sorted match {
      case List(a, b) => (a, b)
    }
  val (targetY1, targetY2) =
    List(limits(9).toInt, limits(11).toInt).sorted.reverse match {
      case List(a, b) => (a, b)
    }

  println(
    s"Target X and Y range: ($targetX1, $targetX2) - ($targetY1, $targetY2)"
  )
  var vX = 6
  var vY = 14
  var posX = 0
  var posY = 0
  var maxY = 0
  // as long as we are not past the target range and not yet on target
  while (posX <= targetX2 && posY >= targetY2 && !isInTarget(posX, posY)) {
    posX += vX
    posY += vY
    maxY = Math.max(maxY, posY)
    vX = Math.max(vX - 1, 0)
    vY = vY - 1
  }

  val xVelocities = findXvelocities(1)

  var overallMaxY = 0
  // start with lowest Y velocity that reaches the target bottom in 1 step
  var velY = targetY2
  var totalMatches = 0
  while (velY <= Math.abs(targetY2)) {
    xVelocities.foreach(vx => testVelocity(vx, velY))
    velY += 1
  }
  println(s"The highest altitude reached was $overallMaxY")
  println(s"There are $totalMatches matches")

  def isInTarget(x: Int, y: Int): Boolean = {
    x >= targetX1 && x <= targetX2 && y >= targetY2 && y <= targetY1
  }

  def findXvelocities(vx: Int, acc: List[Int] = List.empty): List[Int] = {
    var vX = vx
    var posX = 0
    var numsteps = 0
    while (posX <= targetX2 && posX < targetX1 && vX > 0) {
      numsteps += 1
      posX += vX
      vX = Math.max(vX - 1, 0)
    }
    if (posX <= targetX2 && posX >= targetX1) {
      findXvelocities(vx + 1, acc.appended(vx))
    } else {
      if (posX < targetX1) {
        // too slow
        findXvelocities(vx + 1, acc)
      } else {
        // overshot, if we took more than 1 step, look for higher velocities, else we're done.
        if (numsteps == 1) acc else findXvelocities(vx + 1, acc)

      }
    }
  }

  def testVelocity(vx: Int, vy: Int): Unit = {
    var vX = vx
    var vY = vy
    var posX = 0
    var posY = 0
    var maxY = 0
    var steps = 0
    while (posX <= targetX2 && posY >= targetY2 && !isInTarget(posX, posY)) {
      steps += 1
      posX += vX
      posY += vY
      maxY = Math.max(maxY, posY)
      vX = Math.max(vX - 1, 0)
      vY = vY - 1
      if (isInTarget(posX, posY)) {
        overallMaxY = Math.max(maxY, overallMaxY)
        totalMatches += 1
      }
    }

  }
}
