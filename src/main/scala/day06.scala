package nl.about42.aoc2021

object day06 extends AOCApp {
  val lines = getInputForDay("06", true)

  val data = lines.head.split(',').toList.map(_.toInt)

  val liveCount: Array[Long] = Array.fill(9)(0)

  data.foreach(age => liveCount(age) = liveCount(age) + 1)

  println(s"${liveCount.toList}")

  val bigDays = 256
  (1 to bigDays).foreach(d => {
    process(d)
    println(s"$d: ${liveCount.sum}")
  })

  def process(d: Int) = {
    val zeroCount = liveCount(0)
    (0 until 8).foreach(age => liveCount(age) = liveCount(age + 1))
    liveCount(8) = zeroCount
    liveCount(6) = liveCount(6) + zeroCount
  }
}
