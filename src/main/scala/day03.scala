package nl.about42.aoc2021

object day03 extends AOCApp {
  //val lines = getInput("03small.txt")
  val lines = getInputForDay("03", true)

  val input = lines.map(_.map(c => if (c == '0') 0 else 1))

  val total = input.size
  val bitCount = lines.head.length

  var gamma = ""
  var epsilon = ""
  (0 until bitCount).foreach(i => {
    val slice = input.map(_(i))
    val ones = slice.sum
    if (ones >= total / 2) {
      gamma += "1"
      epsilon += "0"
    } else {
      gamma += "0"
      epsilon += "1"
    }

  })

  val gammaVal = Integer.parseInt(gamma, 2)
  val epsVal = Integer.parseInt(epsilon, 2)
  println(s"Gamma: $gamma == $gammaVal")
  println(s"Epsilon: $epsilon == $epsVal")
  println(s"product: ${gammaVal * epsVal}")

  // oxygen
  val oxygenList = findOxygen(input).mkString
  val oxygenValue = Integer.parseInt(oxygenList, 2)
  println(s"$oxygenList == $oxygenValue")
  // CO2
  val co2List = findCO2(input).mkString
  val co2Val = Integer.parseInt(co2List, 2)
  println(s"$co2List == $co2Val")
  println(s"product = ${oxygenValue * co2Val}")

  def decidingCount(poolSize: Int): Int = {
    // the +1 gives us the correct value for odd sizes too.
    (poolSize + 1) / 2
  }

  def findOxygen(
      pool: List[IndexedSeq[Int]],
      bitRank: Int = 0
  ): IndexedSeq[Int] = {
    val oneCount = pool.map(_(bitRank)).sum
    val mostCommon = if (oneCount >= decidingCount(pool.size)) 1 else 0
    val numbers = pool.filter(_(bitRank) == mostCommon)
    if (numbers.size == 1) {
      numbers.head
    } else {
      findOxygen(numbers, bitRank + 1)
    }
  }

  def findCO2(
      pool: List[IndexedSeq[Int]],
      bitRank: Int = 0
  ): IndexedSeq[Int] = {
    val oneCount = pool.map(_(bitRank)).sum
    val leastCommon = if (oneCount >= decidingCount(pool.size)) 0 else 1
    val numbers = pool.filter(_(bitRank) == leastCommon)
    if (numbers.size == 1) {
      numbers.head
    } else {
      findCO2(numbers, bitRank + 1)
    }
  }

}
