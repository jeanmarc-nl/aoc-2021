package nl.about42.aoc2021

import scala.collection.mutable.ListBuffer

object day13 extends AOCApp {
  //val lines = getInput("13small.txt")
  //val lines = getInput("13small2.txt")
  val lines = getInputForDay("13", true)

  val coords = ListBuffer[(Int, Int)]()
  val folds = ListBuffer[(Char, Int)]()

  lines.foreach(l => {
    if (l.nonEmpty) {
      if (l.head == 'f') {
        folds.append((l(11), l.split('=')(1).toInt))
      } else {
        val coordinateList = asInts(l)
        coords.append((coordinateList(0), coordinateList(1)))
      }
    }
  })

  val maxX = findMaxFold('x') * 2 + 1
  val maxY = findMaxFold('y') * 2 + 1

  val paper = Array.fill[Char](maxX, maxY)('.')

  coords.foreach(c => paper(c._1)(c._2) = '#')

  println(s"paper is ${paper.length} by ${paper(0).length}")

  println(s"First fold: ${countDots(fold(folds.head, paper))}")

  val result = foldAll(folds.toList, paper)

  println()
  showGrid(transpose(result))

  def foldAll(
      folds: List[(Char, Int)],
      paper: Array[Array[Char]]
  ): Array[Array[Char]] = {
    folds match {
      case Nil =>
        paper
      case f :: tail =>
        foldAll(tail, fold(f, paper))
    }
  }

  def countDots(paper: Array[Array[Char]]): Int = {
    paper.map(_.map(c => if (c == '#') 1 else 0).sum).sum
  }

  def fold(line: (Char, Int), paper: Array[Array[Char]]): Array[Array[Char]] = {
    val maxX = paper.length
    val maxY = paper(0).length
    val foldLine = line._2
    println(s"Folding ${line._1} at ${line._2} maxX = $maxX, maxY = $maxY ")
    if (line._1 == 'x') {
      (1 to foldLine).foreach(step => {
        val from = foldLine + step
        val to = foldLine - step
        (0 until maxY).foreach(y =>
          paper(to)(y) = foldCell(paper(from)(y), paper(to)(y))
        )
      })
      // return folded result (excluding the fold line)
      paper.take(foldLine)
    } else {
      (1 to foldLine).foreach(step => {
        val from = foldLine + step
        val to = foldLine - step
        (0 until maxX).foreach(x => {
          paper(x)(to) = foldCell(paper(x)(from), paper(x)(to))
        })
      })
      // return folded result (excluding the fold line)
      paper.map(r => r.take(foldLine))
    }
  }

  def findMaxFold(axis: Char) = {
    folds.filter(f => f._1 == axis).map(_._2).max
  }

  def foldCell(a: Char, b: Char): Char = if (a == '#' || b == '#') '#' else a

  def showGrid(g: Array[Array[Char]]) = {
    g.foreach(r => {
      r.foreach(e => print(if (e == '#') '#' else ' '))
      println()
    })
  }
}
