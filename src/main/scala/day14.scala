package nl.about42.aoc2021

import scala.collection.mutable

object day14 extends AOCApp {
  //val lines = getInput("14small.txt")
  val lines = getInput("aoc2021_14_input.txt")

  var seed = lines(0)

  var steps = lines.drop(2).map(l => l.split(" -> ").toList)
  println(s"${steps(0)}")

  var rules = steps.map(r => (r(0), r(1))).toMap
  println(s"${rules}")

  val gen1 = doStep(seed)
  println(gen1)

  val gen10 = generate(seed, 10)

  val counts = gen10.groupBy(identity).view.mapValues(_.length).toMap
  println(s"$counts")

  val min = counts.values.min
  val max = counts.values.max

  println(s"max - min: $max - $min = ${max - min}")

  def generate(seed: String, steps: Int): String = {
    if (steps > 0)
      generate(doStep(seed), steps - 1)
    else
      seed
  }

  def doStep(seed: String): String = {
    seed
      .sliding(2)
      .map(p => if (rules.contains(p)) s"${p(0)}${rules(p)}" else p)
      .mkString + (if (rules.contains(seed.takeRight(2))) seed.takeRight(1)
                   else "")
  }
}

object day14_2 extends AOCApp {
  //val lines = getInput("14small.txt")
  val lines = getInputForDay("14", true)

  var seed = lines.head.sliding(2).toList

  var steps = lines.drop(2).map(l => l.split(" -> ").toList)

  var rules = steps.map(r => (r(0), r(1))).toMap

  // create counter for each possible pair
  val allPairs = rules.keys.map(p => (p, 0L)).toMap
  val pairs = allPairs.to(mutable.Map)

  seed.foreach(p => pairs(p) += 1)
  println(s"$pairs")

  val gen40 = doSteps(pairs.toMap, 40)

  println(s"$gen40")

  showScore(gen40)

  def doSteps(gen: Map[String, Long], count: Int): Map[String, Long] = {
    if (count == 0) {
      gen
    } else {
      doSteps(doStep(gen), count - 1)
    }
  }

  def doStep(gen: Map[String, Long]): Map[String, Long] = {
    val nextGen = allPairs.to(mutable.Map)
    gen.foreach(p => {
      val (p1, p2) = getNextGen(p._1)
      nextGen(p1) = nextGen(p1) + p._2
      nextGen(p2) = nextGen(p2) + p._2
    })
    val result = nextGen.toMap
    showScore(result)
    result
  }

  def getNextGen(in: String): (String, String) = {
    val c = rules(in)
    (s"${in.head}$c", s"$c${in.tail}")
  }

  def showScore(gen: Map[String, Long]) = {
    val letterFrequenciesFromPairs =
      gen.groupBy(_._1.head).view.mapValues(v => v.values.sum).toMap

    // also count the last letter
    val lastChar = lines.head.last
    val letterFrequencies = letterFrequenciesFromPairs.updated(
      lastChar,
      letterFrequenciesFromPairs(lastChar) + 1L
    )

    val min = letterFrequencies.values.min
    val max = letterFrequencies.values.max
    println(s"max - min: $max - $min = ${max - min}")

  }
}
