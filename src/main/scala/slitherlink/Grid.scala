package nl.about42.aoc2021
package slitherlink

import day01.getInput

sealed trait EdgeState
case object On extends EdgeState
case object Off extends EdgeState
case object Unknown extends EdgeState

object Grid {
  def makeGrid(input: List[String]): Grid = {
    val gridSize = input.length / 2
    val data = input.zipWithIndex
      .filter(in => in._2 % 2 == 1)
      .map(_._1)
      .zipWithIndex
      .map(l =>
        (
          l._1.zipWithIndex.filter(el => el._2 % 2 == 1).map(_._1).zipWithIndex,
          l._2
        )
      )
    // build a map of constraints from the input data
    val constraintMap = data
      .flatMap(line =>
        line._1
          .filter(el => el._1 != ' ')
          .map(el => ((el._2, line._2), el._1.asDigit))
      )
      .toMap
    new Grid(gridSize, constraintMap)
  }
}

class Grid(val size: Int, constraints: Map[(Int, Int), Int]) {

  // 3D array, first dim is hor/ver indicator, second dim is X, third dim is Y
  val edges = Array.fill[EdgeState](2, size + 1, size + 1)(Unknown)

  // see if any vertex has 1 incoming line and 1 remaining unknown => it must have a leaving line
  // returns a nonzero value if any update to edges took place
  def updateEdges(): Int = {
    var result = 0
    (0 to size).foreach(x => {
      (0 to size).foreach(y => {
        val edgeState = getVertexEdges(x, y).flatMap(edgOpt =>
          edgOpt.map(edge => getEdgeState(edge))
        )
        val numEdges = edgeState.length
        val onCount = edgeState.count(s => s._2 == On)
        val offCount = edgeState.count(s => s._2 == Off)
        (numEdges, onCount, offCount) match {
          case (2, 1, _) => // must have an outgoing edge
            edgeState.foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = On
            })
            result += 1
          case (2, _, 1) => // cannot have an incoming edge
            edgeState.foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = Off
            })
            result += 1
          case (3, _, 2) => // cannot have an incoming edge
            edgeState
              .filter(es => es._2 == Unknown)
              .foreach(es => {
                edges(es._1._1)(es._1._2)(es._1._3) = Off
              })
            result += 1
          case (3, 1, 1) => // must have outgoing edge
            edgeState
              .filter(es => es._2 == Unknown)
              .foreach(es => {
                edges(es._1._1)(es._1._2)(es._1._3) = On
              })
            result += 1
          case (3, 2, 0) => // cannot have another incoming edge
            edgeState
              .filter(es => es._2 == Unknown)
              .foreach(es => {
                edges(es._1._1)(es._1._2)(es._1._3) = Off
              })
            result += 1
          case (4, 2, 0) => // cannot have another incoming edge
            edgeState
              .filter(es => es._2 == Unknown)
              .foreach(es => {
                edges(es._1._1)(es._1._2)(es._1._3) = Off
              })
            result += 1
          case (4, 1, 2) => // remaining edge must be turned on
            edgeState
              .filter(es => es._2 == Unknown)
              .foreach(es => {
                edges(es._1._1)(es._1._2)(es._1._3) = On
              })
            result += 1
          case _ => // no further action possible
        }
      })
    })
    result
  }

  // returns the number of still unresolved constraints
  def processConstraints(): Int = {
    var result = 0

    constraints.foreach(c => {
      val edgeState = getCellEdges(c._1._1, c._1._2).map(e => getEdgeState(e))
      val onCount = edgeState.count(s => s._2 == On)
      val offCount = edgeState.count(s => s._2 == Off)
      (
        c._2,
        onCount,
        offCount
      ) match {
        case (0, 0, 4) => // no action
        case (1, 1, 3) => // no action
        case (2, 2, 2) => // no action
        case (3, 3, 1) => // no action
        case (4, 4, 0) => // no action
        case (0, _, _) => // at least one is not yet off
          edgeState.foreach(es => {
            edges(es._1._1)(es._1._2)(es._1._3) = Off
          })
        case (1, 1, _) => // remaining unknowns must be off
          edgeState
            .filter(es => es._2 == Unknown)
            .foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = Off
            })
        case (1, _, 3) => // remaining unknown must be on
          edgeState
            .filter(es => es._2 == Unknown)
            .foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = On
            })
        case (
              1,
              on,
              off
            ) => // check if there is a path of 2 along the cell and disable it
          ???
        case (2, 2, _) => // remaining unknowns must be off
          edgeState
            .filter(es => es._2 == Unknown)
            .foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = Off
            })
        case (2, _, 2) => // remaining unknown must be on
          edgeState
            .filter(es => es._2 == Unknown)
            .foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = On
            })
        case (3, 3, _) => // remaining unknowns must be off
          edgeState
            .filter(es => es._2 == Unknown)
            .foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = Off
            })
        case (3, _, 1) => // remaining unknown must be on
          edgeState
            .filter(es => es._2 == Unknown)
            .foreach(es => {
              edges(es._1._1)(es._1._2)(es._1._3) = On
            })
        case (
              3,
              on,
              off
            ) => // check if there is a path of 2 along the cell and enable it
          ???
        case default =>
          //println(s"Constraint has not yet be resolved: $default")
          result += 1
      }

    })

    result
  }

  def getEdgeState(
      edge: (Int, Int, Int)
  ): ((Int, Int, Int), EdgeState) = {
    (edge, edges(edge._1)(edge._2)(edge._3))
  }

  // North, East, South, West
  def getCellEdges(x: Int, y: Int): List[(Int, Int, Int)] = {
    List((0, x, y), (1, x + 1, y), (0, x, y + 1), (1, x, y))
  }

  def getVertexEdges(x: Int, y: Int): List[Option[(Int, Int, Int)]] = {
    val north = if (y == 0) {
      None
    } else {
      Some((1, x, y - 1))
    }
    val east = if (x == size) {
      None
    } else {
      Some((0, x, y))
    }
    val south = if (y == size) {
      None
    } else {
      Some((1, x, y))
    }
    val west = if (x == 0) {
      None
    } else {
      Some((0, x - 1, y))
    }
    List(north, east, south, west)
  }

  def show: Unit = {
    (0 to size).foreach(y => {
      (0 until size).foreach(x => {
        print("+")
        edges(0)(x)(y) match {
          case Unknown => print("...")
          case On      => print("---")
          case Off     => print("   ")
        }
      })
      println("+")
      if (y < size) {
        (0 to size).foreach(x => {
          edges(1)(x)(y) match {
            case Unknown => print(":")
            case On      => print("|")
            case Off     => print(" ")
          }
          print(s" ${constraints.getOrElse((x, y), " ")} ")
        })
        println()
      }
    })
  }
}

object Solver extends App {
  val lines = getInput("slitherlink.txt")

  val grid = Grid.makeGrid(lines)
  grid.show
  var openConstraints = -1
  var latestResult = 0
  var growCount = 0
  while (openConstraints != latestResult || growCount != 0) {
    openConstraints = latestResult
    latestResult = grid.processConstraints()
    println(s"There are still $latestResult open constraints")
    growCount = grid.updateEdges()
    println(s"We have added $growCount edges")
    grid.show
  }
  println(s"No change since last evaluation, done?")
}
