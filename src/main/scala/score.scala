package nl.about42.aoc2021

import json.ScoreDataParser._
import scoredata.{Board, DayScore}

import com.typesafe.config.ConfigFactory
import io.circe.parser._

import java.net.{HttpURLConnection, URL}
import scala.collection.mutable
import scala.io.Source
import scala.sys.exit

object score extends AOCApp {
  val config = ConfigFactory.load("connection.properties")
  val baseUrl = config.getString("baseUrl")
  val cookie = config.getString("cookie")
  val boardId = config.getString("boardId")

  val cache = "score_cache.json"
  val uri =
    s"$baseUrl/leaderboard/private/view/$boardId.json"
  println(s"Fetching data for leaderboard $boardId...")

  val useCache = true
  val data = if (useCache) getJsonFromFile(cache) else getJson(uri)

  val parsedScores = parse(data) match {
    case Left(e) =>
      println(s"Failed to parse json: $e")
      exit()
    case Right(d) => d
  }

  val scoreData = decode[Board](data) match {
    case Left(e) =>
      println(s"Failed to create ScoreData: ${e.getMessage}")
      exit()
    case Right(d) => d
  }

  // create some views on the data
  val starBattle = Array.fill[List[(String, Long, String)]](25, 2)(List.empty)

  scoreData.members.values.foreach(m => {
    m.completion_day_level.foreach(day => {
      day._2.foreach(star => {
        val dayIdx = day._1.toInt - 1
        val starIdx = star._1.toInt - 1
        starBattle(dayIdx)(starIdx) = starBattle(dayIdx)(starIdx)
          .appended(
            (
              m.name.getOrElse(m.id).split(' ').head,
              star._2.get_star_ts,
              m.id
            )
          )
          .sortBy(_._2)
      })
    })
  })

  // normalize the timestamps
  val DAY_IN_SECONDS = 24 * 60 * 60
  starBattle.iterator.zipWithIndex.foreach { case (row, day) =>
    row.iterator.zipWithIndex.foreach { case (col, star) =>
      starBattle(day)(star) = col.map(e =>
        (e._1, e._2 - scoreData.startTS - day * DAY_IN_SECONDS, e._3)
      )
    }
  }

  val numberOfContestants = scoreData.members.size
  println(s"There are $numberOfContestants contestants")
  val localScore = Array.ofDim[Int](numberOfContestants)
  val localIndex = scoreData.members.keys.zipWithIndex.toMap

  // build the maps with daily results
  // key is day and memberid
  val dayScores = mutable.Map[(Int, String), DayScore]()

  starBattle.iterator.zipWithIndex.foreach { case (row, rowInd) =>
    val day = rowInd + 1
    row.iterator.zipWithIndex.foreach { case (col, colInd) =>
      val star = colInd + 1
      col.iterator.zipWithIndex.foreach { case (entry, rank) =>
        localScore(localIndex(entry._3)) =
          localScore(localIndex(entry._3)) + numberOfContestants - rank

        if (star == 2) {
          // update entry in map to include star2 results
          val dayScore = dayScores((day, entry._3))
          dayScores((day, entry._3)) =
            dayScore.copy(time2 = Some(entry._2), rank2 = Some(rank + 1))
        } else {
          // insert entry in map with star1 results
          val dayScore =
            DayScore(entry._3, entry._1, day, rank + 1, entry._2, None, None)
          dayScores((day, entry._3)) = dayScore
        }
      }
    }
  }

  (1 to 25).foreach(day => {
    println(s"Results for day $day")
    val data =
      scoreData.members.keys.map(id => dayScores.get((day, id))).toList.flatten
    val star1Data =
      data.map(ds => (ds.id, ds.name, ds.rank1, ds.time1)).sortBy(e => e._3)
    val star2Data = data
      .filter(_.rank2.nonEmpty)
      .map(ds => (ds.id, ds.name, ds.rank2.get, ds.time2.get))
      .sortBy(e => e._3)
    val overallData = data
      .map(ds =>
        (
          ds.id,
          ds.name,
          ds.getOverallScore(numberOfContestants),
          ds.time1,
          ds.getLastStar
        )
      )
      .sortBy(e =>
        e._3 * -10000000000L + e._5
      ) // highest rank, break ties on oldest timestamp
    if (data.nonEmpty) {
      println(
        "    Overall                        Star 1                              Star 2"
      )
      overallData.zipWithIndex.foreach { case (data, i) =>
        print(f"${i + 1}%2d ${data._3}%3d - ${data._2}%-20s |")
        print(showTime(star1Data(i)._4))
        print(f" - ${star1Data(i)._2}%-20s |")
        if (star2Data.length > i) {
          print(showTime(star2Data(i)._4))
          print(s" - ${star2Data(i)._2} (${showTime(data._5 - data._4).trim})")
        }
        println()
      }
    } else {
      println("No data available")
    }
  })

  println("Local score overall ranking")
  val localRank = localIndex
    .map(e =>
      (
        scoreData.members(e._1).name.getOrElse(e._1),
        localScore(localIndex(e._1))
      )
    )
    .toList
    .sortBy(_._2)
    .reverse
  localRank.foreach(r => println(f"${r._2}%5d - ${r._1}"))

  def getDelta(id: String, day: Int): Long = {
    val member = scoreData.members
      .filter(m => m._2.id == id)
      .head
      ._2

    val s1 = member.completion_day_level(day.toString).get("1")
    val s2 = member.completion_day_level(day.toString).get("2")
    (s1, s2) match {
      case (Some(s1), Some(s2)) => s2.get_star_ts - s1.get_star_ts
      case _                    => 0
    }
  }

  def showTime(ts: Long): String = {
    val hours = ts / 3600
    val minutes = (ts - hours * 3600) / 60
    val seconds = ts % 60
    f"${hours}%5d:${minutes}%02d:${seconds}%02d".takeRight(12)
  }

  def getJson(uri: String): String = {
    val connection = new URL(uri).openConnection.asInstanceOf[HttpURLConnection]
    connection.setRequestProperty("cookie", s"session=$cookie")
    val result = cacheResult(
      Source.fromInputStream(connection.getInputStream).mkString,
      "src/main/resources/" + cache
    )
    connection.getInputStream.close()
    connection.disconnect()
    result
  }
  def getJsonFromFile(file: String): String = {
    getInput(file).mkString
  }
}
