package nl.about42.aoc2021

import scala.collection.mutable

class DeterministicDie(val sides: Int) {
  var count = -1
  def roll: Int = {
    count += 1
    1 + (count % sides)
  }
  def throws = count + 1
}
object day21 extends AOCApp {
  val lines = getInputForDay("21", true)
  //val lines = getInput("21small.txt")

  var p1pos = lines(0).split(' ')(4).toInt - 1
  var p2pos = lines(1).split(' ')(4).toInt - 1

  println("Part 1")
  var p1score = 0
  var p2score = 0
  println(s"Start p1 = $p1pos, p2 = $p2pos")
  val boardSize = 10
  val die = new DeterministicDie(100)

  var p1turn = true
  // use zero based board
  while (p1score < 1000 && p2score < 1000) {
    val turn = die.roll + die.roll + die.roll
    if (p1turn) {
      p1pos = (p1pos + turn) % boardSize
      p1score += (1 + p1pos)
      //println(s"P1 gets $turn, total $p1score, now at $p1pos")
    } else {
      p2pos = (p2pos + turn) % boardSize
      p2score += (p2pos + 1)
      //println(s"P2 gets $turn, total $p2score, now at $p2pos")
    }
    p1turn = !p1turn
  }

  println(s"Done, p1 turn is $p1turn")
  if (p1turn) {
    println(
      s"P1 last score is $p1score, die was thrown ${die.throws} times which makes ${p1score * die.throws}"
    )
  } else {
    println(
      s"P2 last score is $p2score, die was thrown ${die.throws} times which makes ${p2score * die.throws}"
    )
  }

  println()
  println("Part 2")

  // keep track of number of universes where state (p1pos, p2pos, p1score, p2score) occurs
  // from these, advance in any of the possible ways, or count the results if they are a win

  val possibleMoves = Array(3, 4, 5, 6, 7, 8, 9)
  val frequency = Array[Int](1, 3, 6, 7, 6, 3, 1)

  var universes = mutable.Map[(Int, Int, Int, Int), Long]()

  val p1 = lines(0).split(' ')(4).toInt
  val p2 = lines(1).split(' ')(4).toInt

  universes.update((p1, p2, 0, 0), 1)

  var p1wins: Long = 0
  var p2wins: Long = 0
  var done = false
  p1turn = true
  while (!done) {
    // flips back to false if any set of universes has a game in progress
    done = true
    val nextGen = mutable.Map[(Int, Int, Int, Int), Long]()
    // advance all current universes
    universes.foreach { case ((p1, p2, s1, s2), count) =>
      if (Math.max(s1, s2) < 21) {
        done = false
        // play the game for all possible moves
        possibleMoves.indices.foreach(idx => {
          val (pos1, pos2, score1, score2) = if (p1turn) {
            val newPos = move(p1, possibleMoves(idx))
            (newPos, p2, s1 + newPos, s2)
          } else {
            val newPos = move(p2, possibleMoves(idx))
            (p1, newPos, s1, s2 + newPos)
          }
          nextGen((pos1, pos2, score1, score2)) =
            nextGen.getOrElse((pos1, pos2, score1, score2), 0L) +
              frequency(idx) * count
        })
      } else {
        // this group is done, push it into the nextGen
        nextGen((p1, p2, s1, s2)) =
          nextGen.getOrElse((p1, p2, s1, s2), 0L) + count
      }
    }
    universes = nextGen
    println(s"Now have ${universes.size} groups of universes, done is $done")
    p1turn = !p1turn
  }

  universes.foreach { case ((p1, p2, s1, s2), count) =>
    if (s1 >= 21) p1wins += count else p2wins += count
  }
  println(
    s"Done, there are ${universes.size} universes, p1 wincount is $p1wins, p2 wincount is $p2wins, max is ${Math
      .max(p1wins, p2wins)}"
  )

  // a one based board, use modulo after adjusting and then readjust
  def move(pos: Int, steps: Int): Int = {
    1 + ((pos - 1 + steps) % boardSize)
  }

}
