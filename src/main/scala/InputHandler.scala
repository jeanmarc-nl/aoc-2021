package nl.about42.aoc2021

import com.typesafe.config.ConfigFactory

import java.io.{File, FileWriter}
import java.net.{HttpURLConnection, URL}
import scala.io.Source
import scala.reflect.ClassTag
import scala.util.Using

trait AOCApp extends App with InputHandler

trait InputHandler {
  val resourceDir = "src/main/resources"

  def getInputForDay(day: String, useCache: Boolean): List[String] = {
    if (useCache) {
      println(s"Getting day $day from local filesystem...")
      getInput(s"aoc2021_${day}_input.txt")
    } else {
      println(s"Getting day $day from AOC site...")
      // drop any leading zero
      val aocDay = day.toInt.toString
      getInputFromUrl(aocDay)
    }
  }

  def getInputFromUrl(day: String): List[String] = {

    val filename = f"aoc2021_${day.toInt}%02d_input.txt"

    val config = ConfigFactory.load("connection.properties")
    val baseUrl = config.getString("baseUrl")
    val cookie = config.getString("cookie")
    val connection = new URL(s"$baseUrl/day/$day/input").openConnection
      .asInstanceOf[HttpURLConnection]
    connection.setRequestProperty("cookie", s"session=$cookie")
    val result = cacheResult(
      Source.fromInputStream(connection.getInputStream).mkString,
      s"${resourceDir}/$filename"
    )
    println("closing connection")
    connection.getInputStream.close()
    connection.disconnect()
    result.split('\n').toList
  }

  def cacheResult(str: String, cacheLocation: String): String = {
    val fileWriter = new FileWriter(new File(cacheLocation))
    println(s"Updating cache $cacheLocation...")
    fileWriter.write(str)
    fileWriter.close()
    str
  }

  def getInput(file: String): List[String] = {
    Using.resource(Source.fromFile(s"${resourceDir}/${file}")) { source =>
      source.getLines().toList
    }
  }

  def asInts(in: String, splitter: String = ","): List[Int] =
    in.split(splitter).toList.map(_.trim.toInt)

  def asInts(in: List[String]): List[Int] = in.map(_.toInt)

  def intGrid(in: List[String]): Array[Array[Int]] =
    in.map(_.toArray.map(_.asDigit)).toArray

  def showGrid(g: Array[Array[Int]]) = {
    g.foreach(r => {
      r.foreach(e => print(f"$e%2d"))
      println()
    })
  }

  def showGrid[T](g: Array[Array[T]]) = {
    g.foreach(r => {
      r.foreach(e => print(e.toString))
      println()
    })
  }

  def transpose[T: ClassTag](in: Array[Array[T]]): Array[Array[T]] = {
    val res = Array.ofDim[T](in(0).length, in.length)

    in(0).indices.foreach(y => {
      in.indices.foreach(x => res(y)(x) = in(x)(y))
    })
    res
  }
}
